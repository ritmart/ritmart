// Scripts

$(document).ready(function(){
    $('.menu-button').unbind('click').bind('click', function(){
        if ($(this).hasClass('menu-button-active')) {
            $('body').removeClass('overflow');
            $(this).removeClass('menu-button-active');
            $(this).closest('header').find('.overlay-menu').css({'transform':'translate3d(0, -100%, 0)'});
            $(this).closest('header').find('.overlay-menu').queue(function(){
                $(this).delay(200).fadeOut(230);
            });
            $('.logo').delay(100).removeClass('invert');
        } else {
            $('body').addClass('overflow');
            $(this).addClass('menu-button-active');
            $(this).closest('header').find('.overlay-menu').fadeIn('fast').css({'transform':'translate3d(0, 0, 0)'});
            $('.logo').delay(100).addClass('invert');
        }
    })
    $('[data-type="position"]').each(function(){
        var $obj = $(this);
        $(window).scroll(function() {
            var yPos = -($(window).scrollTop() / $obj.data('speed'));
            var coords = yPos + 'px';
            $obj.css({ 'top' : coords });
        });
    });
    $('[data-type="from"]').each(function(){
        var $obj = $(this);
        $(window).scroll(function() {
            var from = $obj.data('from');
            var yPos = -($(window).scrollTop() / $obj.data('speed'));
            var coords = (from - yPos) + 'px';
            $obj.css({ 'bottom' : coords });
        });
    })
})